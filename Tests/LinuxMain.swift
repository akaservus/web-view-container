import XCTest

import WebViewContainerTests

var tests = [XCTestCaseEntry]()
tests += WebViewContainerTests.allTests()
XCTMain(tests)
