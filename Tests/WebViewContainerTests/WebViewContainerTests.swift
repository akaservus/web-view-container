import XCTest
@testable import WebViewContainer

final class WebViewContainerTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(WebViewContainer().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
