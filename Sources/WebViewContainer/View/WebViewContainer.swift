import Foundation
import UIKit
import SwiftUI
import Combine
import WebKit

// MARK: - WebViewHandlerDelegate
// For printing values received from web app
protocol WebViewHandlerDelegate {
    func postMessage(value: String)
}

public struct WebViewContainer {
    public struct WebView: UIViewRepresentable {
        public typealias UIViewType = WKWebView
        
        var path: String
        // Viewmodel object
        @ObservedObject var viewModel: WebViewViewModel = WebViewViewModel()
        
        // Make a coordinator to co-ordinate with WKWebView's default delegate functions
        public func makeCoordinator() -> Coordinator {
            return Coordinator(self)
        }
        
        public init(_ path: String) {
            self.path = path
        }
        
        public func makeUIView(context: UIViewRepresentableContext<WebView>) -> WKWebView {
            // Enable javascript in WKWebView
            let preferences = WKPreferences()
            preferences.javaScriptEnabled = true

            let configuration = WKWebViewConfiguration()
            
            let script = WKUserScript(source: javascript(), injectionTime: .atDocumentStart, forMainFrameOnly: false)
            configuration.userContentController.addUserScript(script)
            
            // Here "iOSNative" is our delegate name that we pushed to the website that is being loaded
            configuration.userContentController.add(self.makeCoordinator(), name: "WebContainer")
            configuration.preferences = preferences

            let webView = WKWebView(frame: CGRect.zero, configuration: configuration)
            webView.navigationDelegate = context.coordinator
            webView.allowsBackForwardNavigationGestures = true
            webView.scrollView.isScrollEnabled = true
            return webView
        }

        public func updateUIView(_ webView: WKWebView, context: Context) {
            if let url = URL(string: path) {
                let directory = url.deletingLastPathComponent().absoluteString
                let name = url.deletingPathExtension().lastPathComponent
                let ext = url.pathExtension
                let scheme = url.scheme
                if scheme == "http" || scheme == "https" {
                    // Load a public website, for example I used here google.com
                    if let url = URL(string: path) {
                        webView.load(URLRequest(url: url))
                    }
                } else if !name.isEmpty {
                    // Load local website
                    if let url = Bundle.main.url(forResource: name, withExtension: ext, subdirectory: directory) {
                        webView.loadFileURL(url, allowingReadAccessTo: url.deletingLastPathComponent())
                    }
                }
            }
        }
        
        public class Coordinator : NSObject, WKNavigationDelegate {
            var parent: WebView
            var delegate: WebViewHandlerDelegate?
            var valueSubscriber: AnyCancellable? = nil
            var webViewNavigationSubscriber: AnyCancellable? = nil
            
            init(_ uiWebView: WebView) {
                self.parent = uiWebView
                self.delegate = parent
            }
            
            deinit {
                valueSubscriber?.cancel()
                webViewNavigationSubscriber?.cancel()
            }
            
            public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
                // Get the title of loaded webcontent
                webView.evaluateJavaScript("document.title") { (response, error) in
                    if let error = error {
                        print("Error getting title")
                        print(error.localizedDescription)
                    }
                    
                    guard let title = response as? String else {
                        return
                    }
                    
                    self.parent.viewModel.showWebTitle.send(title)
                }
                
                // Page loaded so no need to show loader anymore
                self.parent.viewModel.showLoader.send(false)
            }
            
            /* Here I implemented most of the WKWebView's delegate functions so that you can know them and
             can use them in different necessary purposes */
            
            public func webViewWebContentProcessDidTerminate(_ webView: WKWebView) {
                // Hides loader
                parent.viewModel.showLoader.send(false)
            }
            
            public func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
                // Hides loader
                parent.viewModel.showLoader.send(false)
            }
            
            public func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
                // Shows loader
                parent.viewModel.showLoader.send(true)
                /* An observer that observes 'viewModel.valuePublisher' to get value from TextField and
                 pass that value to web app by calling JavaScript function */
                valueSubscriber = self.parent.viewModel.publisher.receive(on: RunLoop.main).sink(receiveValue: { value in
                    let encoder = JSONEncoder()
                    guard let data = try? encoder.encode(value) else {
                        print("Kann nicht serialisieren")
                        return
                    }
                    let javascriptFunction = "WebViewContainer.callback('\(String(decoding: data, as: UTF8.self))');"
                    webView.evaluateJavaScript(javascriptFunction) { (response, error) in
                        if let error = error {
                            print("Error calling javascript:WebViewContainer.callback")
                            print(error.localizedDescription)
                        } else {
                            print("Called javascript:WebViewContainer.callback")
                        }
                    }
                })
            }
            
            public func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
                // Shows loader
                parent.viewModel.showLoader.send(true)
                self.webViewNavigationSubscriber = self.parent.viewModel.navigationPublisher.receive(on: RunLoop.main).sink(receiveValue: { navigation in
                    switch navigation {
                        case .backward:
                            if webView.canGoBack {
                                webView.goBack()
                            }
                        case .forward:
                            if webView.canGoForward {
                                webView.goForward()
                            }
                        case .reload:
                            webView.reload()
                    }
                })
            }
            
            // This function is essential for intercepting every navigation in the webview
            public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
                // Suppose you don't want your user to go a restricted site
                // Here you can get many information about new url from 'navigationAction.request.description'
                if let host = navigationAction.request.url?.host {
                    if host == "restricted.com" {
                        // This cancels the navigation
                        decisionHandler(.cancel)
                        return
                    }
                }
                // This allows the navigation
                decisionHandler(.allow)
            }
        }
    }
}

// MARK: - Extensions
extension WebViewContainer.WebView.Coordinator: WKScriptMessageHandler {
    public func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        // Make sure that your passed delegate is called
        if message.name == "WebContainer", let body = message.body as? String {
            delegate?.postMessage(value: body)
        }
    }
}

extension WebViewContainer.WebView: WebViewHandlerDelegate {
    func postMessage(value: String) {
        print("String Request von WebView: \(value)")
        let decoder = JSONDecoder()
        do {
            let request = try decoder.decode(JavascriptObject.self, from:value.data(using: .utf8)!)
            self.viewModel.consumeMessage(request: request)
        } catch {
            print("Kein valider Request bekommen")
        }
    }
}
