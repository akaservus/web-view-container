//
//  WebViewViewModel.swift
//  WebViewContainer
//
//  Created by Andrey Karachev on 21.05.20.
//

import Foundation
import Combine

enum WebViewNavigation {
    case backward, forward, reload
}

class WebViewViewModel: ObservableObject {
    var navigationPublisher = PassthroughSubject<WebViewNavigation, Never>()
    var showWebTitle = PassthroughSubject<String, Never>()
    var showLoader = PassthroughSubject<Bool, Never>()
    var publisher = PassthroughSubject<JavascriptObject, Never>()
    
    func consumeMessage(request: JavascriptObject) {
        // Response: { result: {result:"Alles Ok", error: null}, id: data.id };
        let result = ["result":"Alles Ok", "error": nil]
        let response = JavascriptObject(id: request.id, method: request.method, message: result)
        
        publisher.send(response)
    }
}
