//
//  JavascriptObject.swift
//  WebViewContainer
//
//  Created by Andrey Karachev on 01.06.20.
//

import Foundation

// Message Objekt fest definieren und Codable nutzen?
struct JavascriptObject: Codable {
    let id: String
    let method: String
    let message: [String: String?]?
}
